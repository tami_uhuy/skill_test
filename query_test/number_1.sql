
CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal_order` datetime NOT NULL,
  `status_pelunasan` enum('lunas','pending') NOT NULL,
  `tanggal_pembayaran` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('2011-11-27 10:12:38.0','pending',NULL)
,('1975-07-07 15:58:28.0','lunas','1975-07-08 15:58:28.0')
,('1994-11-15 01:33:32.0','pending',NULL)
,('1972-11-27 17:55:46.0','lunas','1972-11-28 17:55:46.0')
,('2014-11-08 18:06:10.0','pending',NULL)
,('1975-04-13 19:30:26.0','pending',NULL)
,('1979-01-28 19:07:47.0','pending',NULL)
,('1973-07-10 01:34:26.0','lunas','1973-07-11 01:34:26.0')
,('2008-10-19 08:05:58.0','lunas','2008-10-20 08:05:58.0')
,('2007-01-09 18:46:49.0','pending',NULL)
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('1972-10-13 10:09:29.0','lunas','1972-10-14 10:09:29.0')
,('2015-12-02 14:41:01.0','lunas','2015-12-03 14:41:01.0')
,('1976-12-20 12:48:36.0','lunas','1976-12-21 12:48:36.0')
,('2016-02-06 22:18:23.0','pending',NULL)
,('1991-12-22 02:59:05.0','lunas','1991-12-23 02:59:05.0')
,('2001-03-22 16:35:15.0','pending',NULL)
,('2016-10-28 17:36:03.0','pending',NULL)
,('1984-10-21 09:15:05.0','pending',NULL)
,('1975-10-21 14:46:45.0','lunas','1975-10-22 14:46:45.0')
,('2001-05-23 11:33:30.0','pending',NULL)
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('1974-07-07 09:18:53.0','lunas','1974-07-08 09:18:53.0')
,('1980-03-04 04:57:58.0','pending',NULL)
,('1980-01-12 09:56:18.0','lunas','1980-01-13 09:56:18.0')
,('1972-12-27 09:43:18.0','pending',NULL)
,('2010-03-06 09:35:24.0','pending',NULL)
,('1974-06-18 15:31:38.0','lunas','1974-06-19 15:31:38.0')
,('2018-12-14 00:20:06.0','lunas','2018-12-15 00:20:06.0')
,('1986-02-26 05:14:11.0','pending',NULL)
,('2015-05-25 23:08:58.0','lunas','2015-05-26 23:08:58.0')
,('2011-01-26 08:15:50.0','lunas','2011-01-27 08:15:50.0')
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('1978-12-20 06:07:45.0','lunas','1978-12-21 06:07:45.0')
,('1989-10-23 07:39:53.0','lunas','1989-10-24 07:39:53.0')
,('2020-06-15 08:11:25.0','lunas','2020-06-16 08:11:25.0')
,('2010-02-18 15:06:31.0','pending',NULL)
,('2015-01-03 02:28:09.0','pending',NULL)
,('2004-10-31 23:26:00.0','pending',NULL)
,('2008-03-19 07:16:08.0','pending',NULL)
,('2007-10-22 02:03:29.0','pending',NULL)
,('2011-11-11 16:46:24.0','lunas','2011-11-12 16:46:24.0')
,('1995-07-29 23:56:45.0','lunas','1995-07-30 23:56:45.0')
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('2000-06-30 03:42:44.0','lunas','2000-07-01 03:42:44.0')
,('2004-03-22 21:07:23.0','lunas','2004-03-23 21:07:23.0')
,('1993-03-06 00:29:45.0','pending',NULL)
,('1990-04-14 03:52:44.0','pending',NULL)
,('1986-08-02 09:37:42.0','lunas','1986-08-03 09:37:42.0')
,('1970-09-05 06:34:34.0','lunas','1970-09-06 06:34:34.0')
,('2009-04-13 04:35:59.0','pending',NULL)
,('2013-06-15 01:04:49.0','pending',NULL)
,('2007-08-24 19:17:49.0','pending',NULL)
,('2002-07-18 11:01:33.0','lunas','2002-07-19 11:01:33.0')
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('2016-07-21 04:28:15.0','lunas','2016-07-22 04:28:15.0')
,('2001-01-18 08:26:01.0','lunas','2001-01-19 08:26:01.0')
,('1979-11-19 02:50:53.0','lunas','1979-11-20 02:50:53.0')
,('2009-06-07 07:53:44.0','pending',NULL)
,('1992-09-13 10:50:51.0','lunas','1992-09-14 10:50:51.0')
,('1976-11-07 07:28:51.0','lunas','1976-11-08 07:28:51.0')
,('1997-06-07 21:28:28.0','lunas','1997-06-08 21:28:28.0')
,('1986-02-21 15:56:05.0','pending',NULL)
,('2013-08-30 08:03:10.0','pending',NULL)
,('2009-10-23 04:56:45.0','lunas','2009-10-24 04:56:45.0')
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('2004-12-10 03:53:08.0','pending',NULL)
,('2015-12-11 09:46:56.0','pending',NULL)
,('1985-08-15 15:28:19.0','pending',NULL)
,('1983-05-15 03:03:58.0','lunas','1983-05-16 03:03:58.0')
,('1972-09-25 23:23:02.0','pending',NULL)
,('1993-07-13 19:05:30.0','pending',NULL)
,('1995-09-27 09:00:23.0','pending',NULL)
,('2007-06-26 20:46:41.0','lunas','2007-06-27 20:46:41.0')
,('2012-06-13 21:58:36.0','pending',NULL)
,('1970-09-16 00:53:49.0','pending',NULL)
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('1994-01-20 19:16:34.0','pending',NULL)
,('2012-04-01 15:45:38.0','lunas','2012-04-02 15:45:38.0')
,('1990-03-28 04:41:12.0','pending',NULL)
,('1999-06-30 21:37:29.0','pending',NULL)
,('1981-01-11 17:43:23.0','lunas','1981-01-12 17:43:23.0')
,('2020-04-07 23:23:53.0','pending',NULL)
,('1978-03-18 01:21:11.0','pending',NULL)
,('1989-09-01 13:07:41.0','lunas','1989-09-02 13:07:41.0')
,('1993-07-06 01:29:40.0','pending',NULL)
,('1976-11-23 09:43:08.0','pending',NULL)
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('1982-09-17 14:23:57.0','lunas','1982-09-18 14:23:57.0')
,('2014-11-16 02:15:44.0','lunas','2014-11-17 02:15:44.0')
,('2012-04-12 11:47:43.0','pending',NULL)
,('2018-11-08 09:22:28.0','lunas','2018-11-09 09:22:28.0')
,('1986-07-04 10:14:43.0','lunas','1986-07-05 10:14:43.0')
,('2020-05-16 07:07:40.0','lunas','2020-05-17 07:07:40.0')
,('2019-05-31 01:57:47.0','lunas','2019-06-01 01:57:47.0')
,('1975-08-29 23:51:23.0','lunas','1975-08-30 23:51:23.0')
,('1995-08-14 06:17:34.0','lunas','1995-08-15 06:17:34.0')
,('1992-10-23 17:39:01.0','pending',NULL)
;
INSERT INTO transactions (tanggal_order,status_pelunasan,tanggal_pembayaran) VALUES
('2006-12-07 17:29:58.0','pending',NULL)
,('1986-02-02 11:26:07.0','pending',NULL)
,('1982-05-25 23:44:22.0','lunas','1982-05-26 23:44:22.0')
,('2017-11-06 07:31:34.0','pending',NULL)
,('1985-05-26 17:01:26.0','lunas','1985-05-27 17:01:26.0')
,('2006-09-24 18:15:39.0','pending',NULL)
,('1980-05-27 13:41:16.0','pending',NULL)
,('1977-03-20 03:46:28.0','pending',NULL)
,('1987-05-03 01:38:13.0','lunas','1987-05-04 01:38:13.0')
,('2020-01-09 01:30:45.0','pending',NULL)
;
