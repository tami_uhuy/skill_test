select
t.id,
t.tanggal_order ,
t.status_pelunasan ,
t.tanggal_pembayaran ,
total,
jumlah_barang
from transactions t
join (
	select
		id_transaksi ,
		sum(jumlah) as jumlah_barang,
		sum(subtotal ) as total
	from detail_transactions  group by id_transaksi
) dt on dt.id_transaksi = t.id ;
